package com.example.BookStore.BookConfig;

import com.example.BookStore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadBook {
    @Autowired
    private BookService bookService;

    @Bean
    public void pullBook(){
        bookService.pullBook();
    }
}
